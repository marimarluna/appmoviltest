/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import PropTypes from 'prop-types';
import {View, TouchableWithoutFeedback} from 'react-native';

function RadioButton(props) {
  return (
    <TouchableWithoutFeedback onPress={() => props.onPress()}>
      <View style={{flexDirection: 'row'}}>
        <View
          style={[
            {
              height: props.outerCircleSize,
              width: props.outerCircleSize,
              borderRadius: props.outerCircleSize / 2 || 12,
              borderWidth: props.outerCircleWidth,
              borderColor: props.outerCircleColor,
              backgroundColor: props.backgroundColor,
              alignItems: 'center',
              justifyContent: 'center',
              marginRight: 3,
            },
          ]}>
          {props.currentValue ? (
            <View
              style={{
                height: props.innerCircleSize,
                width: props.innerCircleSize,
                borderRadius: props.innerCircleSize / 2 || 6,
                backgroundColor: props.innerCircleColor,
              }}
            />
          ) : null}
        </View>
        {props.children}
      </View>
    </TouchableWithoutFeedback>
  );
}

RadioButton.defaultProps = {
  outerCircleSize: 22,
  outerCircleWidth: 1,
  outerCircleColor: '#ddd',
  backgroundColor: '#FFF',
  innerCircleSize: 10,
  innerCircleColor: '#160056',
};

RadioButton.propTypes = {
  onPress: PropTypes.func.isRequired,
  outerCircleSize: PropTypes.number,
  outerCircleWidth: PropTypes.number,
  outerCircleColor: PropTypes.string,
  backgroundColor: PropTypes.string,
  innerCircleSize: PropTypes.number,
  innerCircleColor: PropTypes.string,
};

export default RadioButton;
