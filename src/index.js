import React, {Component} from 'react';
import {Platform, SafeAreaView, View} from 'react-native';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/lib/integration/react';
import {store, persistor} from './store';
import Navigator from './Navigator';

export default class App extends Component<{}> {
  componentDidMount() {
    try {
      console.log('open');
    } catch (e) {
      console.log(e);
    }
  }

  componentWillUnmount() {
    // stop listening for events
  }

  render() {
    const prefix =
      Platform.OS === 'android' ? 'project://project/' : 'project://';

    return (
      <SafeAreaView style={{flex: 1}}>
        <Provider store={store}>
          <PersistGate persistor={persistor}>
            <Navigator uriPrefix={prefix} />
          </PersistGate>
        </Provider>
      </SafeAreaView>
    );
  }
}
