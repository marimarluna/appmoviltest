/* eslint-disable prettier/prettier */
import React, {Component} from 'react';
import {
  Text,
  View,
  TextInput,
  TouchableHighlight,
  Keyboard,
  Alert,
} from 'react-native';
import RadioButton from '@components/RadioButton';
import UserService from '@utils/User';
import { connect } from 'react-redux';
import styles from './styles';

import {
  login,
} from '../../store/User';

const mapStateToProps = state => ({
  user: state.user,
});

const mapDispatchToProps = dispatch => ({
  doLogin: (email, token, name) => dispatch(login(email, token, name)),
});

@connect(mapStateToProps, mapDispatchToProps)
export default class RegistreScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userEmail: '',
      userPassword: '',
      userName: '',
      error: '',
      termandcon: false,
    };
  }

  onChangeTextName(text){
    this.setState({userName: text, error: false});
  }
  onChangeTextEmail(text){
    this.setState({userEmail: text, error: false});
  }
  onChangeTextPassword(text){
    this.setState({userPassword: text, error: false});
  }

  validate(){
    const {userEmail, userPassword, userName, termandcon } = this.state;
    if (userEmail === '' || userPassword === '' || userName === '') {
      this.setState({error: 'Complete todos los campos'});
      return false;
    } else if (!termandcon) {
      this.setState({error: 'Es necesario aceptar terminos y condiciones'});
      return false;
    }
    else {
      return true;
    }
  }

  registre(){
    const {userEmail, userPassword, userName } = this.state;
    Keyboard.dismiss();
    if (this.validate()){
      UserService.registre(userEmail.toLowerCase(), userPassword, userName).then((data) => {
        Alert.alert(
          'Exito',
          'Usuario Creado Exitosamente',
          [
            {
              text: 'Ir a Home',
              onPress: () => this.props.navigation.navigate('Home'),
              style: 'cancel',
            },
            {text: 'Iniciar sesión automaticamente ?', onPress: () => this.login()},
          ],
          {cancelable: false},
        );
      }).catch((error) => {
        alert(error.response.error);
      });
    } else {
      console.log('No FUNCIONO');
    }
  }

  login(){
    const { userEmail, userPassword } = this.state;
    const { doLogin } = this.props;
    UserService.login(userEmail.toLowerCase(), userPassword).then((data) => {
      doLogin(data.email, data.token, data.name);
      this.props.navigation.navigate('Home');
    }).catch((error) => {
      alert(error)
    });
  }

  render() {
    const {userEmail, userPassword, userName, error, termandcon } = this.state;
    return (
      <View style={styles.container}>
        <TextInput
        style={styles.textInput}
        onChangeText={text => this.onChangeTextName(text)}
        value={userName}
        placeholder={'Nombre'}
        />
        <TextInput
        style={styles.textInput}
        onChangeText={text => this.onChangeTextEmail(text)}
        value={userEmail}
        placeholder={'Correo Electronico'}
        />
        <TextInput
        style={styles.textInput}
        onChangeText={text => this.onChangeTextPassword(text)}
        value={userPassword}
        placeholder={'Contraseña'}
        secureTextEntry
        />
        <RadioButton currentValue={termandcon} onPress= {() => this.setState({termandcon: !termandcon }) }>
          <View>
            <Text style={styles.textName}>Acepto Terminos y Condiciones</Text>
          </View>
        </RadioButton>
        <Text style={styles.textError}>{error}</Text>
        <TouchableHighlight onPress={() => this.registre()} style={styles.bigButton}>
            <Text style={styles.textButton} >Enviar</Text>
        </TouchableHighlight>
      </View>
    );
  }
}
