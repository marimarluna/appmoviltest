import {StyleSheet} from 'react-native';
import Metrics from '@theme/Metrics';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingVertical: 60,
    alignItems: 'center'
  },
  bigButton: {
    backgroundColor: '#160056',
    height: 40,
    width: Metrics.WIDTH - 40,
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
  },
  textButton: {
    color: '#FFFFFF',
  },
  textInput: {
    height: 40,
    width: Metrics.WIDTH - 30,
    borderWidth: 1,
    paddingHorizontal: 15,
    marginBottom: 15,
  },
  textError: {
    color: 'red',
    marginBottom: 10,
  },
});
export default styles;
