import React, { Component } from 'react';
import { Text, View, TextInput, TouchableHighlight } from 'react-native';
import { connect } from 'react-redux';
import {
  logout,
  update,
} from '../../store/User';
import UserService from '@utils/User';

import styles from './styles';


const mapDispatchToProps = dispatch => ({
  doUpdate: (name) => dispatch(update(name)),
  doLogout: () => dispatch(logout())
});

const mapStateToProps = state => ({
  user: state.user,
});

@connect(mapStateToProps, mapDispatchToProps)
export default class ProfileScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      userName: props.user.name,
      userPassword: '',
    }
  }

  onChangeTextName(text){
    this.setState({userName: text});
  }

  onChangeTextPassword(text){
    this.setState({userPassword: text })
  }

  validate(){
    const {userName, userPassword } = this.state;
    const { user } = this.props;
    if (( userName !== '' && userName !== user.name ) || userPassword !== '') {
      return true;s
    } else {
      return false;
    }
  }

  update(){
    const {userName, userPassword } = this.state;
    const {navigation, user, doUpdate} = this.props;

    if (this.validate()) {
      UserService.updateUser(user.email, userName, userPassword, user.token ).then((data) => {
        console.log(data.name !== user.name);
        if (data.name !== user.name) {
          doUpdate(data.name);
          alert("Cambio de perfil exitoso");
          navigation.navigate('Home');
        }
      }).catch(error => console.log(error));
    } else {
      alert('No hay datos que cambiar');
    }
  }

  logout(){
    this.props.doLogout()
    this.props.navigation.navigate('Home');
  }

  render() {
    const {userPassword, userName, error } = this.state;
    return (
      <View style={styles.container}>
        <TextInput
        style={styles.textInput}
        onChangeText={text => this.onChangeTextName(text)}
        value={userName}
        placeholder={'Nombre'}
        />
        <TextInput
        style={styles.textInput}
        onChangeText={text => this.onChangeTextPassword(text)}
        value={userPassword}
        placeholder={'Contraseña'}
        secureTextEntry
        />
        <Text style={styles.textError}>{error}</Text>
        <TouchableHighlight onPress={() => this.update()} style={styles.bigButton}>
            <Text style={styles.textButton} >Enviar</Text>
        </TouchableHighlight>
        <TouchableHighlight onPress={() => this.logout()} style={styles.bigButton}>
            <Text style={styles.textButton} >Cerrar Sesion</Text>
        </TouchableHighlight>
      </View>
    );
  }
}
