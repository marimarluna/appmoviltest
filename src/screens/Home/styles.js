import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    paddingVertical: 60,
    justifyContent: 'center',
  },
  containerButton: {
    height: 100,
    justifyContent: 'space-between',
  },
  bigButton: {
    backgroundColor: '#160056',
    height: 40,
    width: 130,
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
  },
  textButton: {
    color: '#FFFFFF',
  },
  textWelcome: {
    fontSize: 20,
    marginBottom: 15,
  },
});
export default styles;
