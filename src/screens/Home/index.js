import React, { Component } from 'react';
import { 
  Text, 
  View, 
  TouchableHighlight
} from 'react-native';
import { connect } from 'react-redux';
import styles from './styles';
import {
  logout,
} from '../../store/User';

const mapStateToProps = state => ({
  user: state.user,
});

const mapDispatchToProps = dispatch => ({
  doLogout: () => dispatch(logout())
});

@connect(mapStateToProps,mapDispatchToProps)
export default class LoginScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }
  
  render() {
    const {user} = this.props
    console.log(user);
    return (
      <View style={styles.container}>
        <Text style={styles.textWelcome}>Bienvenido {this.props.user.isAuth ? this.props.user.name : ''}</Text>
        <View style={styles.containerButton}>
          {!user.isAuth &&
          <TouchableHighlight onPress={() => this.props.navigation.navigate('Registre')} style={styles.bigButton}>
            <Text style={styles.textButton} >Registro</Text>
          </TouchableHighlight>}
          {!user.isAuth &&
          <TouchableHighlight onPress={() => this.props.navigation.navigate('Login')} style={styles.bigButton}>
            <Text style={styles.textButton} >Inicio de Sesion</Text>
          </TouchableHighlight>}
          {user.isAuth && 
          <TouchableHighlight onPress={() => this.props.navigation.navigate('Profile' )} style={styles.bigButton}>
            <Text style={styles.textButton} >Editar Perfil</Text>
          </TouchableHighlight>}
          {user.isAuth && 
          <TouchableHighlight onPress={() => this.props.doLogout()} style={styles.bigButton}>
            <Text style={styles.textButton} >Cerrar Sesion</Text>
          </TouchableHighlight>}
        </View>
        </View>    
      );
    }
}
