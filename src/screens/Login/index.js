import React, { Component } from 'react';
import { Text, View, TextInput, TouchableHighlight } from 'react-native';
import { connect } from 'react-redux';
import {
  login,
} from '../../store/User';
import UserService from '@utils/User';

import styles from './styles';

const mapStateToProps = state => ({
  user: state.user,
});

const mapDispatchToProps = dispatch => ({
  doLogin: (email, token, name) => dispatch(login(email, token, name)),
});

@connect(mapStateToProps, mapDispatchToProps)
export default class LoginScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      userEmail: '',
      userPassword: ''
    }
  }

  onChangeTextEmail(text){
    this.setState({userEmail: text, error: false});
  }
  onChangeTextPassword(text){
    this.setState({userPassword: text, error: false});
  }

  login(){
    const { userEmail, userPassword } = this.state;  
    const { doLogin } = this.props;
    UserService.login(userEmail.toLowerCase(), userPassword).then((data) => {
      doLogin(data.email, data.token, data.name);
      this.props.navigation.navigate('Home');
    }).catch((error) => {
      console.log(error);
    });
  }

  render() {
    const {userEmail, userPassword, error } = this.state;
    return (
      <View style={styles.container}>
        <TextInput
        style={styles.textInput}
        onChangeText={text => this.onChangeTextEmail(text)}
        value={userEmail}
        placeholder={'Correo Electronico'}
        />
        <TextInput
        style={styles.textInput}
        onChangeText={text => this.onChangeTextPassword(text)}
        value={userPassword}
        placeholder={'Contraseña'}
        secureTextEntry
        />
        <Text style={styles.textError}>{error}</Text>
        <TouchableHighlight onPress={() => this.login()} style={styles.bigButton}>
            <Text style={styles.textButton} >Enviar</Text>
        </TouchableHighlight>
      </View>
    );
  }
}
