import React, { Component } from 'react';
import { connect, Provider } from 'react-redux';
import { createSwitchNavigator, createStackNavigator } from 'react-navigation';
import NavigationService from './utils/navigation';
import LoginScreen from './screens/Login';
import HomeScreen from './screens/Home';
import ProfileScreen from './screens/Profile';
import RegistreScreen from './screens/Registre';

const AppNavigator = createStackNavigator(
  {
    Home: HomeScreen,
    Login: LoginScreen,
    Profile: ProfileScreen,
    Registre: RegistreScreen,
  },
  {
    initialRouteName: 'Home',
    navigationOptions: ({ navigation }) => {
      return {
        headerTitle: navigation.state.routeName,
      }
    }
  },
);

const mapStateToProps = state => ({
  user: state.user,
});

@connect(mapStateToProps)
export default class Navigator extends Component {
  constructor(props) {
    super(props);
    this.state={
    }
  }

  render() {
    const { uriPrefix } = this.props;
    return (
      <AppNavigator
        uriPrefix={uriPrefix}
        ref={(navigatorRef) => {
         NavigationService.setTopLevelNavigator(navigatorRef);
        }}
      />
    );
  }
}
