import UserStateReducer from './User';

const reducers = {
  user: UserStateReducer,
};

export default reducers;
