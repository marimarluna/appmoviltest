import { createStore, applyMiddleware, compose } from 'redux';
import { persistStore, persistCombineReducers } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import axios from 'axios';
import axiosMiddleware from 'redux-axios-middleware';
import thunk from 'redux-thunk';

import rootReducer from './reducer';

const isProduction = !__DEV__;

// Creating store
const storeConfig = {
  key: 'root', // key is required
  storage,
};
const client = axios.create({
  baseURL: 'https://api.github.com',
  responseType: 'json',
});

const reducer = persistCombineReducers(storeConfig, rootReducer);
let middleware = null;

if (isProduction) {
  middleware = applyMiddleware(axiosMiddleware(client));
} else {
  middleware = applyMiddleware(axiosMiddleware(client));
}
const persistConfig = { middleware };

const store = createStore(
  reducer,
  applyMiddleware(thunk),
);
const persistor = persistStore(store, {}, () => {
  console.log('persist');
});
export { store, persistor, persistConfig };
