export const LOGIN = 'User/LOGIN';
export const UPDATE = 'User/UPDATE';
export const LOGOUT = 'User/LOGOUT';
export const REFRESHING = 'User/REFRESHING';

const INITIAL_STATE = {
  isAuth: false,
  email: null,
  token: null,
  name: null,
};

export const login = (email, token, name) => dispatch => {
  dispatch({
    type: LOGIN,
    email,
    token,
    name,
  });
};

export const update = (name) => dispatch => {
  dispatch({
    type: UPDATE,
    name,
  });
};
export const logout = () => (dispatch, getState) => {
  dispatch({type: LOGOUT});
};

export default function UserStateReducer(state = INITIAL_STATE, action = {}) {
  switch (action.type) {
    case LOGIN:
      return {
        ...state,
        email: action.email,
        token: action.token,
        name: action.name,
        isAuth: true,
      };
    case UPDATE:
      return {
        ...state,
        name: action.name,
      };
    case LOGOUT:
      return {
        ...INITIAL_STATE,
      };
    default:
      return INITIAL_STATE;
  }
}
