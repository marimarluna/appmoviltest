import axios from 'axios';

export default class UserService {
  static registre(email, password, name) {
    return new Promise((resolve, reject) => {
      axios({
        method: 'POST',
        url: 'https://apitesttest.000webhostapp.com/api/register',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        data: {email, password, name},
      })
        .then(response => resolve(response.data))
        .catch(error => {
          reject({error: error.message});
        });
    });
  }
  static login(email, password) {
    return new Promise((resolve, reject) => {
      axios({
        method: 'POST',
        url: 'https://apitesttest.000webhostapp.com/api/login',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        data: {email, password},
      })
        .then(response => resolve(response.data))
        .catch(error => {
          reject({error: error.message});
        });
    });
  }

  static updateUser(email, name, password, token) {
    return new Promise((resolve, reject) => {
      axios({
        method: 'POST',
        url: 'https://apitesttest.000webhostapp.com/api/edit',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
        data: {email, name, password},
      })
        .then(response => resolve(response.data))
        .catch(error => {
          reject({error: error.message});
        });
    });
  }
}
